﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models;

namespace TaskManagementSystem.Tests.ModelTests.BoardTests
{
    [TestClass]
   public class StoryTests
    {
        [TestMethod]
        public void Constructor_Should()
        {
            var member = new Member(32, "abcde");
            var sut = new Story(30, "abcde", "abcde", Models.Enums.Status.Active, Models.Enums.Priority.High,Models.Enums.Size.Medium,member);
            Assert.IsInstanceOfType(sut, typeof(Story));
        }
        [TestMethod]
        public void Constructor_ShouldAssign()
        {
            var member = new Member(32, "abcde");
            var sut = new Story(30, "abcde", "abcde", Models.Enums.Status.Active, Models.Enums.Priority.High, Models.Enums.Size.Medium, member);
            Assert.AreEqual("abcde", sut.Title);
            Assert.AreEqual("abcde", sut.Description);
            Assert.AreEqual(member, sut.Assignee);
            Assert.AreEqual(Models.Enums.Status.Active, sut.Status);
            Assert.AreEqual(Models.Enums.Priority.High, sut.Priority);
            Assert.AreEqual(Models.Enums.Size.Medium, sut.Size);
            Assert.AreEqual(30, sut.Id);
        }
        [TestMethod]
        public void ShouldThrow_WhenNull()
        {
            var member = new Member(32, "abcde");
            var sut = new Story(30,null, "abcde", Models.Enums.Status.Active, Models.Enums.Priority.High, Models.Enums.Size.Medium, member);
            Assert.ThrowsException<ArgumentNullException>(() => new Story(30, null, "abcde", Models.Enums.Status.Active, Models.Enums.Priority.High, Models.Enums.Size.Medium, member));
            Assert.ThrowsException<ArgumentNullException>(() => new Story(30, "abcde", null, Models.Enums.Status.Active, Models.Enums.Priority.High, Models.Enums.Size.Medium, member));

        }
      [TestMethod]
      [DataRow(-1)]
      [DataRow(3)]
      public void ShouldThrow_WhenPriorityOutOfRange(Models.Enums.Priority value)
      {
          var member = new Member(32, "pesho");         
          Assert.ThrowsException<ArgumentException>(() => new Story(30, null, "abcde", Models.Enums.Status.Active, value, Models.Enums.Size.Large, member));
      
      }
       [TestMethod]
       [DataRow(1)]
       [DataRow(5)]
       public void ShouldThrow_WhenStatusOutOfRange(Models.Enums.Status value)
       {
           var member = new Member(32, "pesho");
           Assert.ThrowsException<ArgumentException>(() => new Story(30, null, "abcde", value, Models.Enums.Priority.High, Models.Enums.Size.Large, member));
      
       }
       [TestMethod]
       [DataRow(-1)]
       [DataRow(3)]
       public void ShouldThrow_WhenSizeOutOfRange(Models.Enums.Size value)
       {
           var member = new Member(32, "pesho");
           Assert.ThrowsException<ArgumentException>(() => new Story(30, null, "abcde", Models.Enums.Status.Active, Models.Enums.Priority.High, value, member));
       
       }
        [TestMethod]
        public void ShouldPrint()
        {
            var member = new Member(32, "pesho");
            var sut = new Story(30, "abcde", "abcde", Models.Enums.Status.InProgress, Models.Enums.Priority.High, Models.Enums.Size.Large,member);

            var expectedString = new StringBuilder();
            expectedString.AppendLine($"Story");
            expectedString.AppendLine($"Id: 30");
            expectedString.AppendLine($"Title: abcde");
            expectedString.AppendLine($"Description: abcde");
            expectedString.AppendLine($"Status: InProgress");
            expectedString.AppendLine($"Priority: High");
            expectedString.AppendLine($"Size: large");
            expectedString.AppendLine($"Assignee: pesho ID: 32");


            Assert.AreEqual(expectedString.ToString().TrimEnd(), sut.ToString());
        }

    }
}
