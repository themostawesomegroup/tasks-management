﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models;

namespace TaskManagementSystem.Tests.ModelTests.BoardTests
{
    [TestClass]
    public class FeedbackTest
    {
        [TestMethod]
        public void Constructor_ShouldCreateFeedback()
        {
            var sut = new Feedback(30, "abcdefghigklmnop", "abcdefghigklmnop", Models.Enums.Status.Scheduled, 6);
            Assert.IsInstanceOfType(sut, typeof(Feedback));
        }
        [TestMethod]
        public void Constructor_ShouldThrow_WhenArgumentIsNull()
        {

            Assert.ThrowsException<ArgumentException>(() => new Feedback(30, null, "abcde", Models.Enums.Status.Scheduled, 6));
            Assert.ThrowsException<ArgumentException>(() => new Feedback(30, "abcde", null, Models.Enums.Status.Scheduled, 6));

        }
        [TestMethod]
        public void Contructor_ShouldAssign()
        {
            var sut = new Feedback(30, "abcdefghigklmnop", "abcdefghigklmnop", Models.Enums.Status.Scheduled, 6);

            Assert.AreEqual(30, sut.Id);
            Assert.AreEqual("abcdefghigklmnop", sut.Title);
            Assert.AreEqual("abcdefghigklmnop", sut.Description);
            Assert.AreEqual(Models.Enums.Status.Scheduled, sut.Status);
            Assert.AreEqual(6, sut.Rating);
        }

        [TestMethod]
        [DataRow(3)]
        [DataRow(8)]
        public void ShouldThrow_WhenStatusOutOfRange(Models.Enums.Status value)
        {

            var sut = new Feedback(30, "abcde", "abcde", Models.Enums.Status.Scheduled, 6);
            Assert.ThrowsException<ArgumentException>(() => new Feedback(30, "abcde", "abcde", value, 6));

        }

        [TestMethod]
        [DataRow(0)]
        [DataRow(11)]
        public void ShouldThrow_WhenRatingOutOfRange(int value)
        {         
            Assert.ThrowsException<ArgumentException>(() => new Feedback(30, "abcde", "abcde", Models.Enums.Status.Scheduled, value));

        }
         [TestMethod]
         [DataRow(null)]
         
         public void ShouldThrow_WhenRatingIsNull(int value)
         {
             Assert.ThrowsException<ArgumentException>(() => new Feedback(30, "abcde", "abcde", Models.Enums.Status.Scheduled, value));
         }
        [TestMethod]
        public void ShouldPrint()
        {
            var sut = new Feedback(30, "abcdefghigklmnop", "abcdefghigklmnop", Models.Enums.Status.Scheduled, 6);

            var expectedString = new StringBuilder();
            expectedString.AppendLine($"Feedback");
            expectedString.AppendLine($"Id: 30");
            expectedString.AppendLine($"Title: abcdefghigklmnop");
            expectedString.AppendLine($"Description: abcdefghigklmnop");
            expectedString.AppendLine($"Status: Scheduled");
            expectedString.AppendLine($"Rating: 6");

            Assert.AreEqual(expectedString.ToString().TrimEnd(), sut.ToString());
        }
    }
}
