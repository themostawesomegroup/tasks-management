﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Tests.ModelTests.BoardTests
{
    [TestClass]
    public class MemberTests
    {
        [TestMethod]
        public void ConstructorShould_CreateTypeMember()
        {
            var sut = new Member(15, "abcde");
            Assert.IsInstanceOfType(sut, typeof(Member));
        }
        [TestMethod]
        public void Constructor_ShouldAssign()
        {
            var id = 15;
            var name = "abcde";
            var sut = new Member(id, name);
            Assert.AreEqual(id, sut.Id);
            Assert.AreEqual(name, sut.Name);
        }
        [TestMethod]
        public void ShouldThrow_WhenNameIsNull()
        {
            Assert.ThrowsException<ArgumentNullException>(() => new Member(15, null));
        }
        [TestMethod]
        [DataRow("a")]
        [DataRow("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")]
        public void ShouldThrow_WhenNameIsOutOfRange(string name)
        {
            Assert.ThrowsException<ArgumentException>(() => new Member(15, name));
        }
       
        [TestMethod]
        public void Should_ShowActivityHistory()
        {
            var sut = new Member(14, "abcdefg");
            var task = new Bug(30, "abcedefg", "abcdefg", Models.Enums.Status.Active, Models.Enums.Priority.High, Models.Enums.Severity.Critical, sut);
            sut.AssignTask(task);
            var expectedMessage = new StringBuilder();
            expectedMessage.AppendLine($"Bug");
            expectedMessage.AppendLine($"ID: 14");
            expectedMessage.AppendLine($"Name: abcdefg");
            expectedMessage.AppendLine($"Numer of tasks: 1");
            expectedMessage.AppendLine($"Number of actions in activity history: 1");
            Assert.AreEqual(expectedMessage.ToString(), sut.ShowActivityHistory());
        }
        [TestMethod]
        public void ShouldThrow_WhenListITaskNull()
        {
            var sut = new Member(14, "abcdefg");

            Assert.ThrowsException<ArgumentException>(() => new List<ITask>(null));
        }
        [TestMethod]
        public void ShouldThrow_WhenListILogNull()
        {
            var sut = new Member(14, "abcdefg");

            Assert.ThrowsException<ArgumentException>(() => new List<ILog>(null));
        }
    }
}
