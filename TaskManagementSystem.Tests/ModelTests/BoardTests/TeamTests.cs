﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Tests.ModelTests.BoardTests
{
    [TestClass]
    public class TeamTests
    {
        [TestMethod]
        public void Constructor_Should()
        {
            var sut = new Team("abcde");
            Assert.IsInstanceOfType(sut, typeof(Team));
        }
        [TestMethod]
        public void ShouldThrow_WhenListIsNull()
        {
            Assert.ThrowsException<ArgumentNullException>(() => new List<IBoard>(null));
            Assert.ThrowsException<ArgumentNullException>(() => new List<IMember>(null));
        }
        [TestMethod]
        [DataRow("a")]
        [DataRow("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")]
        public void ShouldThrow_WhenNameIsOutOfRange(string name)
        {
            Assert.ThrowsException<ArgumentException>(() => new Team(name));
        }
        [TestMethod]
        public void ShouldThrow_WhenNameIsNull()
        {
            Assert.ThrowsException<ArgumentException>(() => new Team(null));
        }
        [TestMethod]
        public void Should_AssignName()
        {
            var sut = new Team("abcde");
            Assert.AreEqual("abcde", sut.Name);
        }
        [TestMethod]
        public void ShouldPrint()
        {
            var sut = new Team("abcde");
            var expectedMessage = new StringBuilder();

            expectedMessage.AppendLine($"Team");
            expectedMessage.AppendLine($"Name: abcde");
            expectedMessage.AppendLine($"Number of members: 0");
            expectedMessage.AppendLine($"Number of boards: 0");


            Assert.AreEqual(expectedMessage.ToString(), sut.ToString());
        }
        [TestMethod]
        public void Should_AddBoard()
        {
            var sut = new Team("abcde");
            var board = new Board(32, "abcde");
            sut.AddBoard(board);

            Assert.AreEqual(1, sut.Boards.Count);
        }
        [TestMethod]
        public void Should_RemoveBoard()
        {
            var sut = new Team("Team1 name");
            var board = new Board(32, "asdfgh");
            sut.AddBoard(board);
            sut.RemoveBoard(board);

            Assert.AreEqual(0, sut.Boards.Count);
        }
        [TestMethod]
        public void Should_AddMember()
        {
            var sut = new Team("abcde");
            var member = new Member(33, "abcde");
            sut.AddMember(member);

            Assert.AreEqual(1, sut.Members.Count);
        }

        [TestMethod]
        public void Should_RemoveMember()
        {
            var sut = new Team("abcde");
            var member = new Member(33, "abcde");
            sut.AddMember(member);
            sut.RemoveMember(member);

            Assert.AreEqual(0, sut.Members.Count);
        }
    }
}
