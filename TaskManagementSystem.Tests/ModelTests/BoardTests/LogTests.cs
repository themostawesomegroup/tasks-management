﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models;

namespace TaskManagementSystem.Tests.ModelTests.BoardTests
{
    [TestClass]
    public class LogTests
    {
        [TestMethod]
        public void ContructorShould_CreateLog()
        {
            var sut = new Log("abcde");
            Assert.IsInstanceOfType(sut, typeof(Log));
        }
       [TestMethod]
       [DataRow(1)]
       [DataRow(501)]


        public void ShouldThrow_WhenValidateDescription(int value)
       {
            var title = new string('a', value);
           Assert.ThrowsException<ArgumentException>(() => new Log(title));   
      
       }
        [TestMethod]
        [DataRow(null)]        
        public void ShouldThrow_WhenValidateDescriptionIsNull(string value)
        {
            Assert.ThrowsException<ArgumentException>(() => new Log(value));
       
        }
    }
}
