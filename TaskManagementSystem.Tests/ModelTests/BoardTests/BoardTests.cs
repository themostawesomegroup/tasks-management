﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models;

namespace TaskManagementSystem.Tests.ModelTests.BoardTests
{
    [TestClass]
    public class BoardTests
    {


        [TestMethod]
        public void Constructor_Should_AssignProperBoardName()
        {
            var id = 10;
            var name = "Board1";

            var board = new Board(id, name);

            Assert.AreEqual(name, board.Name);
        }

        [TestMethod]
        public void Name_Should_Throw_When_IsNull()
        {
            var id = 10;

            var board = new Board(id, null);

            Assert.ThrowsException<ArgumentException>(() => new Board(id, null));
        }

        [TestMethod]
        public void Name_Should_Throw_When_IsTooShort()
        {
            var id = 10;
            var name = new string('a', 1);

            Assert.ThrowsException<ArgumentException>(() => new Board(id, name));
        }

        [TestMethod]
        public void Name_Should_Throw_When_IsTooLong()
        {
            var id = 10;
            var name = new string('a', 30);

            Assert.ThrowsException<ArgumentException>(() => new Board(id, name));
        }
    }
}
