﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models;

namespace TaskManagementSystem.Tests.ModelTests.BoardTests
{
    [TestClass]
   public class BugTests
    {
       [TestMethod]
       public void Constructor_ShouldCreateBug()
        {
            var member = new Member(32, "pesho");
            var sut = new Bug(30,"abcde","abcde",Models.Enums.Status.Active,Models.Enums.Priority.High,Models.Enums.Severity.Critical,member);
            Assert.IsInstanceOfType(sut, typeof (Bug));
        }
        [TestMethod]
        public void Constructor_ShouldThrow_WhenArgumentIsNull()
        {
            var member = new Member(32, "pesho");

            Assert.ThrowsException<ArgumentException>(() => new Bug(30, null, "abcde", Models.Enums.Status.Active, Models.Enums.Priority.High, Models.Enums.Severity.Critical, member));
            Assert.ThrowsException<ArgumentException>(() => new Bug(30, "abcde", null, Models.Enums.Status.Active, Models.Enums.Priority.High, Models.Enums.Severity.Critical, member));
            Assert.ThrowsException<ArgumentException>(() => new Bug(30, "abcde", "abcde", Models.Enums.Status.Active, Models.Enums.Priority.High, Models.Enums.Severity.Critical, null));

        }
        [TestMethod]
        public void Contructor_ShouldAssign()
        {
            var member = new Member(32, "pesho");

            var sut = new Bug(30, "abcde", "abcde", Models.Enums.Status.Active, Models.Enums.Priority.High, Models.Enums.Severity.Critical, member);

            Assert.AreEqual(30, sut.Id);
            Assert.AreEqual("abcde", sut.Title);
            Assert.AreEqual("abcde", sut.Description);
            Assert.AreEqual(Models.Enums.Status.Active, sut.Status);
            Assert.AreEqual (Models.Enums.Priority.High, sut.Priority);
            Assert.AreEqual(Models.Enums.Severity.Critical, sut.Severity);
            Assert.AreEqual(member, sut.Assignee);

        }
        
       [TestMethod]
       [DataRow (-1)]
       [DataRow(3)]
       public void ShouldThrow_WhenStatusOutOfRange(Models.Enums.Status value)
       {
           var member = new Member(32, "pesho");
           
           Assert.ThrowsException<ArgumentException>(() => new Bug(30, null, "abcde", value, Models.Enums.Priority.High, Models.Enums.Severity.Critical, member));
      
       }
        [TestMethod]
        [DataRow(-1)]
        [DataRow(3)]
        public void ShouldThrow_WhenPriorityOutOfRange(Models.Enums.Priority value)
        {
            var member = new Member(32, "pesho");
            Assert.ThrowsException<ArgumentException>(() => new Bug(30, null, "abcde", Models.Enums.Status.Active, value, Models.Enums.Severity.Critical, member));
       
        }
       
        [TestMethod]
        public void ShouldPrint()
        {
            var member = new Member(32, "pesho");

            var sut = new Bug(30, "abcde", "abcde", Models.Enums.Status.Active, Models.Enums.Priority.High, Models.Enums.Severity.Critical, member);

            var expectedString = new StringBuilder();
            expectedString.AppendLine($"Bug");
            expectedString.AppendLine($"Id: 30");
            expectedString.AppendLine($"Title: abcde");
            expectedString.AppendLine($"Description: abcde");
            expectedString.AppendLine($"Status: Active");
            expectedString.AppendLine($"Priority: High");
            expectedString.AppendLine($"Severity: Critical");
            expectedString.AppendLine($"Assignee: pesho ID: 32");

            Assert.AreEqual(expectedString.ToString(), sut.ToString());
        }
    }
}
