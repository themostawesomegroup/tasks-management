﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManagementSystem.Models
{
    public class Validator
    {
        public static void ValidateArgumentIsNotNull(object arg, string message)
        {
            if (arg == null)
            {
                throw new ArgumentException(message);
            }
        }

        public static void ValidateCollectionContainsObject<T>(T obj, ICollection<T> collection, string message)
        {
            if (!collection.Contains(obj))
            {
                throw new ArgumentException(message);
            }
        }

        public static void ValidateObjectIsNotInCollection<T>(T obj, ICollection<T> collection, string message)
        {
            if (collection.Contains(obj))
            {
                throw new ArgumentException(message);
            }
        }

        public static void ValidateIntRange(int value, int min, int max, string message)
        {
            if (value < min || value > max)
            {
                throw new ArgumentException(message);
            }
        }

        public static void ValidateStringLength(string value, int min, int max, string message)
        {
            if (value.Length < min || value.Length > max)
            {
                throw new ArgumentException(message);
            }
        }
    }
}
