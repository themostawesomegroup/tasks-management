﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Models
{
    public class Bug : Task, IBug
    {
        private Priority priority;
        private Severity severity;
        private IMember assignee;

        public Bug(int id, string title, string description, Status status, Priority priority, Severity severity, IMember assignee)
            : base(id, title, description, status)
        {
            Priority = priority;
            Severity = severity;
            Assignee = assignee;
        }

        public List<string> StepsToReproduce => throw new NotImplementedException();//TODO

        public Priority Priority
        {
            get => this.priority;
            set
            {

                if (isInitialized)
                {
                    AddToHistoryLog($"Priority changed to {value}.");
                }

                this.priority = value;
            }
        }

        public Severity Severity
        {
            get => this.severity;
            set
            {

                if (isInitialized)
                {
                    AddToHistoryLog($"Severity changed to {value}.");
                }

                this.severity = value;
            }
        }

        public IMember Assignee
        {
            get => this.assignee;
            set
            {
                ValidateAssignee(value);

                if (isInitialized)
                {
                    AddToHistoryLog($"Assignee {value.Name} added.");
                }

                this.assignee = value;
            }
        }

     

        private void ValidateAssignee(IMember newAssignee)
        {
            if (this.assignee == newAssignee)
            {
                throw new ArgumentException("Assignee already assigned!");
            }
        }

        protected override void ValidateStatus(Status status)
        {
            if (status < Status.Active || status > Status.Fixed)
            {
                throw new ArgumentException("Status is not valid!");
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine(base.ToString());
            sb.AppendLine($"Priority: {Priority}");
            sb.AppendLine($"Severity: {Severity}");
            sb.AppendLine($"Assignee: {Assignee.Name} ID: {Assignee.Id}");

            return sb.ToString().TrimEnd();
        }
    }
}
