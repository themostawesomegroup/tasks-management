﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Models
{
    public class Team : ITeam
    {
        private const int NameMinLength = 5;
        private const int NameMaxLength = 15;

        private string name;
        private readonly List<IMember> members;
        private readonly List<IBoard> boards;

        public Team(string name)
        {
            Name = name;
            this.members = new List<IMember>();
            this.boards = new List<IBoard>();
        }

        public string Name
        {
            get => this.name;
            set
            {
                Validator.ValidateArgumentIsNotNull(value, "Argument is null.");
                Validator.ValidateStringLength(value, NameMinLength, NameMaxLength, "Invalid string length of name");

                this.name = value;
            }
        }

        public List<IMember> Members => new List<IMember>(this.members);

        public List<IBoard> Boards => new List<IBoard>(this.boards);

        public void AddBoard(IBoard board)
        {
            Validator.ValidateArgumentIsNotNull(board, "Argument is null.");
            Validator.ValidateObjectIsNotInCollection(board, Boards, "Object is contained in collection");

            boards.Add(board);
        }

        public void AddMember(IMember member)
        {
            Validator.ValidateArgumentIsNotNull(member, "Argument is null.");
            Validator.ValidateObjectIsNotInCollection(member, Members, "Member is in collection.");

            members.Add(member);

        }

        public void RemoveBoard(IBoard board)
        {
            Validator.ValidateArgumentIsNotNull(board, "Argument is null.");
            Validator.ValidateCollectionContainsObject(board, Boards, "Object is not contained in collection");

            boards.Remove(board);
        }

        public void RemoveMember(IMember member)
        {
            Validator.ValidateArgumentIsNotNull(member, "Argument is null.");
            Validator.ValidateCollectionContainsObject(member, Members, "Object is not contained in collection");

            members.Remove(member);
        }
        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"{this.GetType().Name}");
            sb.AppendLine($"Name: {Name}");
            sb.AppendLine($"Number of members: {this.members.Count}");
            sb.AppendLine($"Number of boards: {this.boards.Count}");

            return sb.ToString();
        }
    }
}
