﻿using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Models
{
    public class Comment : IComment
    {
        public Comment(string content , string author)
        {
            ValidateContent(content);
            ValidateAuthor(author);
            Content = content;
            Author = author;
        }

        public string Content { get; }

        public string Author { get; }

        private void ValidateContent(string con)
        {
            Validator.ValidateArgumentIsNotNull(con,"Content is null value!");
            Validator.ValidateStringLength(con,5,500,"Content string length must be between 5 and 500 characters!");
        }

        private void ValidateAuthor(string auth)
        {
            Validator.ValidateArgumentIsNotNull(auth,"Author is null value!");
            Validator.ValidateStringLength(auth,2,50, "Author string length must be between 5 and 500 characters!");
        }



    }
}
