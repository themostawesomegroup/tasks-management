﻿using System.Collections.Generic;

namespace TaskManagementSystem.Models.Contracts
{
    public interface IMember : IHasId
    {
        string Name { get; }
        List<ITask> Tasks { get; }
        List<ILog> ActivityHistory { get; }

        void AddCommentToTask(ITask taskToAddComment, IComment commentToAdd);
        void AssignTask(ITask task);
        void UnassignTask(ITask task);

        string ShowActivityHistory();
        //void AddLog(string description);
        string ToString();
    }
}
