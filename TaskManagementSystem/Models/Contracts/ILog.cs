﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManagementSystem.Models.Contracts
{
    public interface ILog
    {
        public string Description { get; }
        public DateTime Time { get; }
        string ToString();
    }
}
