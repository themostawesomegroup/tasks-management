﻿namespace TaskManagementSystem.Models.Contracts
{
    public interface IHasId
    {
        int Id { get; }
    }
}
