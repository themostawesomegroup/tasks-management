﻿namespace TaskManagementSystem.Models.Contracts
{
    public interface IFeedback : ITask 
    {
        int Rating { get; }

        string ToString();
    }
}
