﻿using System.Collections.Generic;

namespace TaskManagementSystem.Models.Contracts
{
    public interface ITeam
    {
        string Name { get; }
        List<IMember> Members { get; }
        List<IBoard> Boards { get; }

        void AddMember(IMember member);
        void RemoveMember(IMember member);
        void AddBoard(IBoard board);
        void RemoveBoard(IBoard board);
    }
}
