﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskManagementSystem.Models.Contracts
{
    public interface IHasAssignee
    {
        IMember Assignee { get; set; }
    }
}
