﻿using System.Collections.Generic;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Models.Contracts
{
    public interface ITask : IHasId
    {
        string Title { get; }
        string Description { get; }
        Status Status { get; }
        List<ILog> History { get; }

        void AddComment(IComment comment);
        string ToString();
        string ViewHistory();
        string ViewComments();
    }
}
