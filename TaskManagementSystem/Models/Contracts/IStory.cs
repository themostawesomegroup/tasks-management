﻿using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Models.Contracts
{
    public interface IStory : ITask, IHasAssignee
    {
        Priority Priority { get; }
        Size Size { get; }
        //IMember Assignee { get; }

        string ToString();
    }
}
