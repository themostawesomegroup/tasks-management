﻿using System.Collections.Generic;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Models.Contracts
{
    public interface IBug : ITask, IHasAssignee
    {
        List<string> StepsToReproduce { get; }
        Priority Priority { get; }
        Severity Severity { get; }
        //IMember Assignee { get; }

        string ToString();
    }
}
