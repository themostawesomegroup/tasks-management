﻿using System.Collections.Generic;

namespace TaskManagementSystem.Models.Contracts
{
    public interface IBoard : IHasId
    {
        string Name { get; }
        List<ITask> Tasks { get; }
        List<ILog> ActivityHistory { get; }

        void AddTask(ITask task);
        void RemoveTask(ITask task);
    }
}
