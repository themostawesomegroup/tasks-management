﻿namespace TaskManagementSystem.Models.Enums
{
    public enum Severity
    {
        Critical = 0,
        Major = 1,
        Minor = 2
    }
}
