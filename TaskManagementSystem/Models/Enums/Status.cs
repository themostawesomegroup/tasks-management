﻿namespace TaskManagementSystem.Models.Enums
{
    public enum Status
    {
        Active = 0,
        Fixed = 1,
        NotDone = 2,
        InProgress = 3,
        Done = 4,
        New = 5,
        Unscheduled = 6,
        Scheduled = 7
    }
}
