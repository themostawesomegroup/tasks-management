﻿using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Models
{
    public class Board : IBoard
    {
        private const int NameMinLength = 5;
        private const int NameMaxLength = 10;

        private string name;
        private readonly List<ITask> tasks;
        private readonly List<ILog> activityHistory;

        public Board(int id, string name)
        {
            Id = id;
            Name = name;
            this.tasks = new List<ITask>();
            this.activityHistory = new List<ILog>();
            
            AddLog($"Board {name} created.");
        }

        public string Name
        {
            get => this.name;
            set
            {
                Validator.ValidateArgumentIsNotNull(value, "Name is null.");
                Validator.ValidateStringLength(value, NameMinLength, NameMaxLength, "String length of name is invalid.");
                
                this.name = value;
            }
        }

        public int Id { get; }

        public List<ITask> Tasks { get => new List<ITask>(this.tasks); }

        public List<ILog> ActivityHistory { get => new List<ILog>(this.activityHistory); }


        public void AddTask(ITask task)
        {
            Validator.ValidateArgumentIsNotNull(task, "Argument is null.");
            Validator.ValidateObjectIsNotInCollection(task, Tasks, "Object is contained in collection");

            tasks.Add(task);
            AddLog($"task {task.Title} added.");
        }

        public void RemoveTask(ITask task)
        {
            Validator.ValidateArgumentIsNotNull(task, "Argument is null.");
            Validator.ValidateCollectionContainsObject(task, Tasks, "Object is not contained in collection");

            tasks.Remove(task);
            AddLog($"task {task.Title} removed.");
        }

        private void AddLog(string description)
        {
            this.ActivityHistory.Add(new Log(description));
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"{this.GetType().Name}");
            sb.AppendLine($"ID: {Id}");
            sb.AppendLine($"Name: {Name}");
            sb.AppendLine($"Number of tasks: {this.tasks.Count}");
            sb.AppendLine($"Number of actions in activity history: {this.activityHistory.Count}");

            return base.ToString();
        }
    }
}
