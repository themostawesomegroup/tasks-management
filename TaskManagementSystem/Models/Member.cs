﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Models
{
    public class Member : IMember
    {
        // TODO Add error messages

        private const int NameMinLength = 2;
        private const int NameMaxLength = 15;

        private string name;
        private readonly List<ITask> tasks;
        private readonly List<ILog> activityHistory;

        public Member(int id, string name)
        {
            Id = id;
            Name = name;
            AddLog($"{name} created as member.");
            this.tasks = new List<ITask>();
            this.activityHistory = new List<ILog>();
        }

        public int Id { get; }

        public string Name
        {
            get => this.name;
            set
            {
                Validator.ValidateArgumentIsNotNull(value, "Argument is null.");
                Validator.ValidateStringLength(value, NameMinLength, NameMaxLength, "String length is invalid");

                this.name = value;
            }
        }

        public List<ITask> Tasks { get => new List<ITask>(this.tasks); }

        public List<ILog> ActivityHistory { get => new List<ILog>(this.activityHistory); }

        public void AddCommentToTask(ITask taskToAddComment, IComment commentToAdd)
        {
            ValidateComment(commentToAdd);
            taskToAddComment.AddComment(commentToAdd);
            AddLog($"Comment added to task {taskToAddComment.Title}");
        }

        private void AddLog(string description)
        {
            this.ActivityHistory.Add(new Log(description));
        }

        public void AssignTask(ITask task)
        {
            Validator.ValidateArgumentIsNotNull(task, "Argument is null.");
            Validator.ValidateObjectIsNotInCollection(task, Tasks, "Object is contained in collection");

            tasks.Add(task);
            AddLog($"task {task.Title} assigned.");
        }

        public void UnassignTask(ITask task)
        {
            Validator.ValidateArgumentIsNotNull(task, "Argument is null.");
            Validator.ValidateCollectionContainsObject(task, Tasks, "Object is not contained in collection");

            tasks.Remove(task);
            AddLog($"task {task.Title} unassigned.");
        }

        public string ShowActivityHistory()
        {
            if (ActivityHistory.Count == 0)
            {
                return string.Empty;
            }

            var sb = new StringBuilder();

            activityHistory.ForEach(a => sb.AppendLine(a.ToString()));

            return sb.ToString();
        }

        private void ValidateComment(IComment comment)
        {
            Validator.ValidateArgumentIsNotNull(comment, "Argument is null.");
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"{this.GetType().Name}");
            sb.AppendLine($"ID: {Id}");
            sb.AppendLine($"Name: {Name}");
            sb.AppendLine($"Number of tasks: {this.tasks.Count}");
            sb.AppendLine($"Number of actions in activity history: {this.activityHistory.Count}");

            return base.ToString();
        }
    }
}
