﻿using System;
using System.Text;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Models
{
    public class Story : Task, IStory
    {
        private Priority priority;
        private Size size;
        private IMember assignee;

        public Story(int id, string title, string description, Status status, Priority priority, Size size, IMember assignee)
            : base(id, title, description, status)
        {
            Priority = priority;
            Size = size;
            Assignee = assignee;
            AddToHistoryLog("Story created");
        }

        public Priority Priority
        {
            get => this.priority;
            set
            {

                if (isInitialized)
                {
                    AddToHistoryLog($"Priority set to {value}.");
                }

                this.priority = value;
            }
        }

        public Size Size
        {
            get => this.size;
            set
            {

                if (isInitialized)
                {
                    AddToHistoryLog($"Size set to {value}.");
                }

                this.size = value;
            }
        }

        public IMember Assignee
        {
            get => this.assignee;
            set
            {
                ValidateAssignee(value);

                if (isInitialized)
                {
                    AddToHistoryLog($"Assignee set to {value.Name}.");
                }

                this.assignee = value;
            }
        }


        private void ValidateAssignee(IMember newAssignee)
        {
            if (this.assignee == newAssignee)
            {
                throw new ArgumentException("Assignee already assigned!");
            }
        }

        protected override void ValidateStatus(Status status)
        {
            if (status < Status.NotDone || status > Status.Done)
            {
                throw new ArgumentException("Status is not valid.");
            }
        }
        
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine(base.ToString());
            sb.AppendLine($"Priority: {Priority}");
            sb.AppendLine($"Size: {Size}");
            sb.AppendLine($"Assignee: {Assignee.Name} ID: {Assignee.Id}");

            return sb.ToString().TrimEnd();
        }
    }
}
