﻿using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Models
{
    public abstract class Task : ITask
    {
        private const int TitleMinLength = 10;
        private const int TitleMaxLength = 50;
        private const int DescriptionMinLength = 10;
        private const int DescriptionMaxLength = 500;

        protected readonly List<IComment> comments;
        protected readonly List<ILog> history;
        protected Status status;

        protected bool isInitialized; 
        public Task(int id, string title, string description, Status status)
        {
            this.Id = id;
            ValidateTitle(title);
            this.Title = title;
            ValidateDescription(description);
            this.Description = description;
            this.Status = status;

            this.comments = new List<IComment>();
            this.history = new List<ILog>();
            this.isInitialized = true;
        }

        public string Title { get; }

        public string Description { get; }

        public Status Status
        {
            get => this.status;
            set
            {
                ValidateStatus(value);

                if (isInitialized)
                {
                    // TODO
                    AddToHistoryLog($"Status changed to {value}.");
                }

                this.status = value;
            }
        }

        public List<ILog> History => new List<ILog>(this.history);

        public int Id { get; }

        public void AddComment(IComment comment)
        {
            Validator.ValidateArgumentIsNotNull(comment, "Argument is null.");
            Validator.ValidateObjectIsNotInCollection(comment, this.comments, "Object is contained in collection");

            this.comments.Add(comment);
            AddToHistoryLog("Comment added.");
        }

        public string ViewComments()
        {
            if (comments.Count == 0)
            {
                return string.Empty;
            }

            var sb = new StringBuilder();
            comments?.ForEach(c => sb.AppendLine(c.ToString()));
            return sb.ToString();
        }

        public string ViewHistory()
        {
            var sb = new StringBuilder();
            this.history?.ForEach(c => sb.AppendLine(c.ToString()));
            return sb.ToString();
        }

        private void ValidateTitle(string title)
        {
            Validator.ValidateArgumentIsNotNull(title, "Argument is null.");
            Validator.ValidateStringLength(title, TitleMinLength, TitleMaxLength, "String length is invalid");
        }

        private void ValidateDescription(string desc)
        {
            Validator.ValidateArgumentIsNotNull(desc, "Argument is null.");
            Validator.ValidateStringLength(desc, DescriptionMinLength, DescriptionMaxLength, "String length is invalid");
        }

        protected void AddToHistoryLog(string message)
        {
            this.history.Add(new Log(message));
        }
        protected abstract void ValidateStatus(Status status);

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"{this.GetType().Name}");
            sb.AppendLine($"Id: {Id}");
            sb.AppendLine($"Title: {Title}");
            sb.AppendLine($"Description: {Description}");
            sb.AppendLine($"Status: {Status}");

            return sb.ToString().Trim();
        }
    }
}
