﻿using System;
using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Models
{
    public class Log : ILog
    {
        private const int minLength = 2;
        private const int maxLength = 500;
        public Log(string description)
        {
            ValidateDescription(description);
            Description = description;
            this.Time = DateTime.Now;
        }

        public string Description { get; }

        public DateTime Time { get; }

        public override string ToString()
        {
            return $"[{this.Time.ToString("dd-MM-yyyy")}] {this.Description}";
        }

        private void ValidateDescription(string value)
        {
            Validator.ValidateArgumentIsNotNull(value, "Description argument is null!");
            Validator.ValidateStringLength(value, minLength, maxLength, "String length is invalid!");
        }
    }
}
