﻿using System;
using System.Text;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Models
{
    public class Feedback : Task, IFeedback
    {
        // TODO Add ex messages
        private int rating;

        public Feedback(int id, string title, string description, Status status, int rating)
            : base(id, title, description, status)
        {

            Rating = rating;
            AddToHistoryLog("Feedback created.");
        }

        public int Rating
        {
            get => this.rating;
            set
            {
                ValidateRating(value);

                if (isInitialized)
                {
                    // TODO
                    AddToHistoryLog($"Rating changed to {value}.");
                }

                this.rating = value;
            }
        }

        private void ValidateRating(int value)
        {
            Validator.ValidateArgumentIsNotNull(value, "Argument is null.");
            Validator.ValidateIntRange(value, 1, 10, "Int range is invalid");
        }

        protected override void ValidateStatus(Status status)
        {
            if (status < Status.Done || status > Status.Scheduled)
            {
                throw new ArgumentException("Status is not valid.");
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine(base.ToString());
            sb.AppendLine($"Rating: {Rating}");

            return sb.ToString().TrimEnd();
        }
    }
}
