﻿using System;
using TaskManagementSystem.Core;

namespace TaskManagementSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            var repository = new Repository();
            var commandFactory = new CommandFactory(repository);
            var engine = new Engine(commandFactory);

            engine.Start();
        }
    }
}
