﻿using System.Collections.Generic;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Core.Contracts
{
    public interface IRepository
    {
        // Lists
        IList<ITeam> Teams { get; }
        IList<IMember> Members { get; }

        IList<IBoard> Boards { get; }
        IList<ITask> Tasks { get; }

        // Create Methods
        IBug CreateBug(string title, string description, Status status, Priority priority, Severity severity, IMember assignee);
        IStory CreateStory(string title, string description, Status status, Priority priority, Size size, IMember assignee);
        IFeedback CreateFeedback(string title, string description, Status status, int rating);

        ITeam CreateTeam(string name);
        IMember CreateMember(string name);
        IBoard CreateBoard(string boardName, string teamName);

        // Find Methods
        ITeam FindTeamByName(string name);
        IMember FindMemberByName(string name);
        IMember FindMemberById(int id);
        IBoard FindBoardById(int id);
        ITask FindTaskById(int id);

        //Add Methods
        string AddMemberToTeam(IMember member, ITeam team);
        string RemoveMemberFromTeam(IMember member, ITeam team);
    }
}
