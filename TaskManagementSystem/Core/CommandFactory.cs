﻿using TaskManagementSystem.Commands;
using TaskManagementSystem.Commands.CreateCommands;
using TaskManagementSystem.Commands.Contracts;
using TaskManagementSystem.Core.Contracts;

using System;
using System.Collections.Generic;
using TaskManagementSystem.Commands.ShowCommands;
using TaskManagementSystem.Commands.ChangeCommands;
using TaskManagementSystem.Commands.AssignCommands;
using TaskManagementSystem.Commands.AddCommands;

namespace TaskManagementSystem.Core
{
    public class CommandFactory : ICommandFactory
    {
        private readonly IRepository repository;

        public CommandFactory(IRepository repository)
        {
            this.repository = repository;
        }

        public ICommand Create(string commandLine)
        {
            // RemoveEmptyEntries makes sure no empty strings are added to the result of the split operation.
            string[] arguments = commandLine.Split(" ", StringSplitOptions.RemoveEmptyEntries);

            string commandName = this.ExtractCommandName(arguments);
            List<string> commandParameters = this.ExtractCommandParameters(arguments);

            ICommand command;
            switch (commandName.ToLower())
            {
                //create commands
                case "createassignee":
                    {
                        command = new CreateMemberCommand(commandParameters, this.repository);
                        break;
                    }
                case "createteam":
                    {
                        command = new CreateTeamCommand(commandParameters, this.repository);
                        break;
                    }
                case "createboardinteam":
                    {
                        command = new CreateBoardCommand(commandParameters, this.repository);
                        break;
                    }
                case "createbug":
                    {
                        command = new CreateBugCommand(commandParameters, this.repository);
                        break;
                    }
                case "createstory":
                    {
                        command = new CreateStoryCommand(commandParameters, this.repository);
                        break;
                    }
                case "createfeedback":
                    {
                        command = new CreateFeedbackCommand(commandParameters, this.repository);
                        break;
                    }


                //function commands
                case "showallpeople":
                    {
                        command = new ShowAllPeopleCommand(commandParameters, this.repository);
                        break;
                    }
                case "showpersonactivity":
                    {
                        command = new ShowPersonActivityCommand(commandParameters, this.repository);
                        break;
                    }
                case "showteams":
                    {
                        command = new ShowAllTeamsCommand(commandParameters, this.repository);
                        break;
                    }
                case "showteamsactivity":
                    {
                        command = new ShowTeamActivityCommand(commandParameters, this.repository);
                        break;
                    }
                case "showteammembers":
                    {
                        command = new ShowAllTeamMembersCommand(commandParameters, this.repository);
                        break;
                    }
                case "showteamboards":
                    {
                        command = new ShowAllTeamBoardsCommand(commandParameters, this.repository);
                        break;
                    }
                case "showboardactivity":
                    {
                        command = new ShowBoardActivityCommand(commandParameters, this.repository);
                        break;
                    }

                    //assign commands
                case "assigntask":
                    {
                        command = new AssignTaskCommand(commandParameters, this.repository);
                        break;
                    }
                case "unassigntask":
                    {
                        command = new UnassignTaskCommand(commandParameters, this.repository);
                        break;
                    }

                    //add commands
                case "addcommenttotask":
                    {
                        command = new AddCommentToTaskCommand(commandParameters, this.repository);
                        break;
                    }
                case "addmembertoteam":
                    {
                        command = new AddMemberToTeamCommand(commandParameters, this.repository);
                        break;
                    }



                //altering operations
                case "changebugpriority":
                    {
                        command = new ChangeBugPriorityCommand(commandParameters, this.repository);
                        break;
                    }
                case "changebugseverity":
                    {
                        command = new ChangeBugSeverityCommand(commandParameters, this.repository);
                        break;
                    }
                case "changebugstatus":
                    {

                        command = new ChangeBugStatusCommand(commandParameters, this.repository);
                        break;
                    }
                case "changestorypriority":
                    {
                        command = new ChangeStoryPriorityCommand(commandParameters, this.repository);
                        break;
                    }
                case "changestorysize":
                    {
                        command = new ChangeStorySizeCommand(commandParameters, this.repository);
                        break;
                    }
                case "changestorystatus":
                    {
                        command = new ChangeStoryStatusCommand(commandParameters, this.repository);
                        break;
                    }
                case "changefeedbackrating":
                    {
                        command = new ChangeFeedbackRatingCommand(commandParameters, this.repository);
                        break;
                    }
                case "changefeedbackstatus":
                    {
                        command = new ChangeFeedbackStatusCommand(commandParameters, this.repository);
                        break;
                    }



                default:
                    {
                        throw new InvalidOperationException($"Command with name: {commandName} doesn't exist!");
                    }
            }
            return command;
        }

        // Receives a full line and extracts the command to be executed from it.
        // For example, if the input line is "FilterBy Assignee John", the method will return "FilterBy".
        private string ExtractCommandName(string[] arguments)
        {
            string commandName = arguments[0];
            return commandName;
        }

        // Receives a full line and extracts the parameters that are needed for the command to execute.
        // For example, if the input line is "FilterBy Assignee John",
        // the method will return a list of ["Assignee", "John"].
        private List<String> ExtractCommandParameters(string[] arguments)
        {
            List<string> commandParameters = new List<string>();

            for (int i = 1; i < arguments.Length; i++)
            {
                commandParameters.Add(arguments[i]);
            }

            return commandParameters;
        }
    }
}
