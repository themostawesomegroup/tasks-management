﻿using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Contracts;

using System.Collections.Generic;
using TaskManagementSystem.Models.Enums;
using System.Linq;
using System;

namespace TaskManagementSystem.Core
{
    public class Repository : IRepository
    {
        private int nextId;
        private readonly List<ITeam> teams;
        private readonly List<IMember> members;
        private readonly List<IBoard> boards;
        private readonly List<ITask> tasks;

        public Repository()
        {
            this.nextId = 0;

            this.teams = new List<ITeam>();
            this.members = new List<IMember>();
            this.boards = new List<IBoard>();
            this.tasks = new List<ITask>();
        }

        public IList<ITeam> Teams => new List<ITeam>(this.teams);

        public IList<IMember> Members => new List<IMember>(this.members);

        public IList<IBoard> Boards => new List<IBoard>(this.boards);

        public IList<ITask> Tasks => new List<ITask>(this.tasks);

        // Create Methods
        public IBug CreateBug(string title, string description, Status status, Priority priority, Severity severity, IMember assignee)
        {
            var bug = new Bug(++nextId, title, description, status, priority, severity, assignee);
            this.tasks.Add(bug);
            return bug;
        }

        public IStory CreateStory(string title, string description, Status status, Priority priority, Size size, IMember assignee)
        {
            var story = new Story(++nextId, title, description, status, priority, size, assignee);
            this.tasks.Add(story);
            return story;
        }

        public IFeedback CreateFeedback(string title, string description, Status status, int rating)
        {
            var feedback = new Feedback(++nextId, title, description, status, rating);
            this.tasks.Add(feedback);
            return feedback;
        }

        public ITeam CreateTeam(string name)
        {
            if (this.teams.Any(t => t.Name.ToLower() == name.ToLower()))
            {
                // TODO - add message
                throw new ArgumentException();
            }

            var team = new Team(name);
            this.teams.Add(team);
            return team;
        }

        public IMember CreateMember(string name)
        {
            if (this.members.Any(m => m.Name.ToLower() == name.ToLower()))
            {
                // TODO - add message
                throw new ArgumentException();
            }

            var member = new Member(++nextId, name);
            this.members.Add(member);
            return member;
        }

        public IBoard CreateBoard(string boardName, string teamName)
        {
            var team = FindTeamByName(teamName);

            if (team.Boards.Any(b => b.Name.ToLower() == boardName.ToLower()))
            {
                // TODO - add message
                throw new ArgumentException();
            }

            var board = new Board(++nextId, boardName);
            team.AddBoard(board);
            return board;
        }

        // Find Methods
        public ITeam FindTeamByName(string name)
        {
            var team = this.teams.FirstOrDefault(t => t.Name.ToLower() == name.ToLower());

            if (team == null)
            {
                // TODO - add message
                throw new ArgumentException();
            }

            return team;
        }

        public IMember FindMemberByName(string name)
        {
            var member = this.members.FirstOrDefault(m => m.Name.ToLower() == name.ToLower());

            if (member == null)
            {
                // TODO - add message
                throw new ArgumentException();
            }

            return member;
        }

        public IMember FindMemberById(int id)
        {
            var member = this.members.FirstOrDefault(m => m.Id == id);

            if (member == null)
            {
                // TODO - add message
                throw new ArgumentException();
            }

            return member;
        }

        public IBoard FindBoardById(int id)
        {
            var board = this.boards.FirstOrDefault(b => b.Id == id);

            if (board == null)
            {
                // TODO - add message
                throw new ArgumentException();
            }

            return board;
        }

        public ITask FindTaskById(int id)
        {
            var task = this.tasks.FirstOrDefault(t => t.Id == id);

            if (task == null)
            {
                // TODO - add message
                throw new ArgumentException("Task is null.");
            }

            return task;
        }

        public string AddMemberToTeam(IMember member, ITeam team)
        {
            team.AddMember(member);

            return $"Member {member.Name} added to team {team.Name}";

        }

        public string RemoveMemberFromTeam(IMember member, ITeam team)
        {
            team.RemoveMember(member);

            return $"Member {member.Name} removed from team {team.Name}";
        }
    }
}
