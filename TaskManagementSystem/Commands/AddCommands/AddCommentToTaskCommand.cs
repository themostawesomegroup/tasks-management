﻿using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;

namespace TaskManagementSystem.Commands.AddCommands
{
    public class AddCommentToTaskCommand : BaseCommand
    {
        private const int minArguments = 3;

        public AddCommentToTaskCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - task id
            //  [1] - content
            //  [2] - author

            int id = ParseIntParameter(CommandParameters[0], "Id");
            var task = Repository.FindTaskById(id);
            string content = CommandParameters[1];
            string author = CommandParameters[2];
            var comment = new Comment(content, author);

            task.AddComment(comment);

            return $"Comment added to task with ID: {id}";
        }
    }
}
