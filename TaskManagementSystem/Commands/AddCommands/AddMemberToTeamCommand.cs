﻿using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;

namespace TaskManagementSystem.Commands.AddCommands
{
    public class AddMemberToTeamCommand : BaseCommand
    {
        private const int minArguments = 2;

        public AddMemberToTeamCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - member id
            //  [1] - Team name

            int id = ParseIntParameter(CommandParameters[0], "Id");
            string teamName = CommandParameters[1];
            var member = Repository.FindMemberById(id);
            var team = Repository.FindTeamByName(teamName);

            team.AddMember(member);

            return $"Member with ID: {id} added to team: {team.Name}";
        }
    }
}
