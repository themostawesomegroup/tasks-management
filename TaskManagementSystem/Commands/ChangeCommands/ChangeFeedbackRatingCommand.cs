﻿using System;
using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ChangeCommands
{
    public class ChangeFeedbackRatingCommand : BaseCommand
    {
        private const int minArguments = 2;

        public ChangeFeedbackRatingCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - feedback id
            //  [1] - new Rating

            int id = ParseIntParameter(CommandParameters[0], "Id");
            int newRating = ParseIntParameter(CommandParameters[1], "Rating");

            Feedback feedback = Repository.FindTaskById(id) as Feedback;

            if (feedback == null)
            {
                throw new ArgumentException($"Task with ID: {id} is not a Feedback");
            }

            feedback.Rating = newRating;

            return $"Feedback Rating changed to {newRating}.";
        }
    }
}
