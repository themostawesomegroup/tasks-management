﻿using System;
using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ChangeCommands
{
    public class ChangeStorySizeCommand : BaseCommand
    {
        private const int minArguments = 2;

        public ChangeStorySizeCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - story id
            //  [1] - new Size

            int id = ParseIntParameter(CommandParameters[0], "Id");
            Size newSize = ParseEnumOfType<Size>(CommandParameters[1]);

            Story story = Repository.FindTaskById(id) as Story;

            if (story == null)
            {
                throw new ArgumentException($"Task with ID: {id} is not a Story");
            }

            story.Size = newSize;

            return $"Story Size changed to {newSize}.";
        }
    }
}
