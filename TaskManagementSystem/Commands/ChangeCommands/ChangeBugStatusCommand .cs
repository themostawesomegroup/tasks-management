﻿using System;
using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ChangeCommands
{
    public class ChangeBugStatusCommand : BaseCommand
    {
        private const int minArguments = 2;

        public ChangeBugStatusCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - bug id
            //  [1] - new Status

            int id = ParseIntParameter(CommandParameters[0], "Id");
            Status newStatus = ParseEnumOfType<Status>(CommandParameters[1]);

            Bug bug = Repository.FindTaskById(id) as Bug;

            if (bug == null)
            {
                throw new ArgumentException($"Task with ID: {id} is not a Bug");
            }

            bug.Status = newStatus;

            return $"Bug Status changed to {newStatus}.";
        }
    }
}
