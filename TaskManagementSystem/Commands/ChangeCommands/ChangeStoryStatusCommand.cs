﻿using System;
using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ChangeCommands
{
    public class ChangeStoryStatusCommand : BaseCommand
    {
        private const int minArguments = 2;

        public ChangeStoryStatusCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - story id
            //  [1] - new Status

            int id = ParseIntParameter(CommandParameters[0], "Id");
            Status newStatus = ParseEnumOfType<Status>(CommandParameters[1]);

            Story story = Repository.FindTaskById(id) as Story;

            if (story == null)
            {
                throw new ArgumentException($"Task with ID: {id} is not a Story");
            }

            story.Status = newStatus;

            return $"Story Status changed to {newStatus}.";
        }
    }
}
