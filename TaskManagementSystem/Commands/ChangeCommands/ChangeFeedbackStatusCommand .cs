﻿using System;
using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ChangeCommands
{
    public class ChangeFeedbackStatusCommand : BaseCommand
    {
        private const int minArguments = 2;

        public ChangeFeedbackStatusCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - Feedback id
            //  [1] - new Status

            int id = ParseIntParameter(CommandParameters[0], "Id");
            Status newStatus = ParseEnumOfType<Status>(CommandParameters[1]);

            Feedback feedback = Repository.FindTaskById(id) as Feedback;

            if (feedback == null)
            {
                throw new ArgumentException($"Task with ID: {id} is not a Feedback");
            }

            feedback.Status = newStatus;

            return $"Feedback Status changed to {newStatus}.";
        }
    }
}
