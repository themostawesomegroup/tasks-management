﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ShowCommands
{
    public class ShowAllTeamMembersCommand : BaseCommand
    {
        private const int minArguments = 1;

        public ShowAllTeamMembersCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - team name

            string teamName = CommandParameters[0];
            var members = Repository.FindTeamByName(teamName)?.Members;

            if (members == null || members.Count == 0)
            {
                return $"Team {teamName} has no members.";
            }

            var sb = new StringBuilder();

            members.ForEach(t => sb.AppendLine(t.ToString()));

            return "Team members: \n" + sb.ToString();
        }
    }
}
