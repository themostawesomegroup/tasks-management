﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ShowCommands
{
    public class ShowAllTeamsCommand : BaseCommand
    {

        public ShowAllTeamsCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {

            var teams = new List<ITeam>(Repository.Teams);

            if (teams == null || teams.Count == 0)
            {
                return "No Teams.";
            }

            var sb = new StringBuilder();

            teams.ForEach(a => sb.AppendLine(a.ToString()));

            return "Teams: \n " + sb.ToString().Trim();
        }
    }
}
