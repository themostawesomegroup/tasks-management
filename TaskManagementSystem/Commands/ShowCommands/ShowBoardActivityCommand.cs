﻿using System;
using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ShowCommands
{
    public class ShowBoardActivityCommand : BaseCommand
    {
        private const int minArguments = 1;

        public ShowBoardActivityCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - board id

            int id = ParseIntParameter(CommandParameters[0], "board Id");
            var board = Repository.FindBoardById(id);

            if (board.ActivityHistory.Count == 0)
            {
                return "No Board activity history.";
            }

            var sb = new StringBuilder();

            board.ActivityHistory?.ForEach(ah => sb.AppendLine(ah.ToString()));

            return sb.ToString().Trim();
        }
    }
}
