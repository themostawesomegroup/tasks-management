﻿using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Commands.ShowCommands
{
    public class ShowAllPeopleCommand : BaseCommand
    {
        public ShowAllPeopleCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            var members = new List<IMember>(this.Repository.Members);

            if (members.Count == 0)
            {
                return "No members.";
            }

            var sb = new StringBuilder();

            members?.ForEach(m => sb.AppendLine(m.ToString()));

            return sb.ToString().Trim();
        }
    }
}
