﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ShowCommands
{
    public class ShowAllTeamBoardsCommand : BaseCommand
    {
        private const int minArguments = 1;

        public ShowAllTeamBoardsCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - team name

            string teamName = CommandParameters[0];
            var boards = Repository.FindTeamByName(teamName)?.Boards;

            if (boards == null || boards.Count == 0)
            {
                return $"Team {teamName} has no boards.";
            }

            var sb = new StringBuilder();

            boards.ForEach(t => sb.AppendLine(t.ToString()));

            return "Boards: \n" + sb.ToString();
        }
    }
}
