﻿using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Core.Contracts;

namespace TaskManagementSystem.Commands.ShowCommands
{
    public class ShowPersonActivityCommand : BaseCommand
    {
        private const int minArguments = 1;

        public ShowPersonActivityCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - member name

            string name = CommandParameters[0];
            var member = Repository.FindMemberByName(name);

            if (member.ActivityHistory.Count == 0)
            {
                return "No Activity History.";
            }

            var sb = new StringBuilder();

            member.ActivityHistory?.ForEach(ah => sb.AppendLine(ah.ToString()));

            return sb.ToString().Trim();
        }
    }
}
