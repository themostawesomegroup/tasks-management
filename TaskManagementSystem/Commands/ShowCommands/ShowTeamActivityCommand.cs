﻿using System.Collections.Generic;
using System.Text;
using TaskManagementSystem.Core.Contracts;

namespace TaskManagementSystem.Commands.ShowCommands
{
    public class ShowTeamActivityCommand : BaseCommand
    {
        private const int minArguments = 1;

        public ShowTeamActivityCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - team name

            string teamName = CommandParameters[0];
            var team = Repository.FindTeamByName(teamName);
            var sb = new StringBuilder();

            if (team.Boards.Count == 0)
            {
                sb.AppendLine("No Team Boards activity history.");
            }
            else
            {
                sb.AppendLine("Team Boards activity history: ");

                for (int i = 0; i < team.Boards.Count; i++)
                {
                    team.Boards[i].ActivityHistory?.ForEach(ah => sb.AppendLine(ah.ToString()));
                }               
            }

            if (team.Members.Count== 0)
            {
                sb.AppendLine("No Team Members activity history.");
            }
            else
            {
                sb.AppendLine("Team Members activity history: ");

                for (int i = 0; i < team.Members.Count; i++)
                {
                    team.Members[i].ActivityHistory?.ForEach(ah => sb.AppendLine(ah.ToString()));

                }
            }

            return sb.ToString().Trim();
        }
    }
}
