﻿using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;

namespace TaskManagementSystem.Commands.CreateCommands
{
    public class CreateTeamCommand : BaseCommand
    {
        private const int minArguments = 1;

        public CreateTeamCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - team name

            string name = CommandParameters[0];
            var team = this.Repository.CreateTeam(name);

            return $"Team with name {name} was created.";
        }
    }
}
