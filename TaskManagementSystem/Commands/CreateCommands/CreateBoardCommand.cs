﻿using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;

namespace TaskManagementSystem.Commands.CreateCommands
{
    public class CreateBoardCommand : BaseCommand
    {
        private const int minArguments = 2;

        public CreateBoardCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - board name
            //  [1] - team name

            string boardName = CommandParameters[0];
            string teamName = CommandParameters[1];

            var board = Repository.CreateBoard(boardName, teamName);
            
            return $"Board {boardName} (ID: {board.Id}) was created in team {teamName}.";
        }
    }
}
