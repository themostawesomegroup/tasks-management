﻿using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.CreateCommands
{
    public class CreateStoryCommand : BaseCommand
    {
        // TODO
        private const int minArguments = 6;

        public CreateStoryCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters: 
            //  [0] - title
            //  [1] - description
            //  [2] - status
            //  [2] - priority
            //  [2] - size
            //  [2] - assigneeName


            string title = CommandParameters[0];
            string description = CommandParameters[1];
            Status status = ParseEnumOfType<Status>(CommandParameters[2]);
            Priority priority = ParseEnumOfType<Priority>(CommandParameters[3]);
            Size size = ParseEnumOfType<Size>(CommandParameters[4]);
            IMember assigneeName = Repository.FindMemberByName(CommandParameters[5]);

            var story = Repository.CreateStory(title,description,status,priority,size,assigneeName);

            return $"Story {title} (ID: {story.Id}) was created.";
            
        }
    }
}
