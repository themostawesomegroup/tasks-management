﻿using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.CreateCommands
{
    public class CreateFeedbackCommand : BaseCommand
    {
        // TODO
        private const int minArguments = 4;

        public CreateFeedbackCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters: 
            //  [0] - title 
            //  [1] - description
            //  [2] - status
            //  [3] - rating


            string title = CommandParameters[0];
            string description = CommandParameters[1];
            Status status = ParseEnumOfType<Status>(CommandParameters[2]);
            int rating = ParseIntParameter(CommandParameters[3],"Rating");

            var feedback = Repository.CreateFeedback(title,description,status,rating);

            return $"Feedback {title} (ID: {feedback.Id}) was created.";

        }
    }
}
