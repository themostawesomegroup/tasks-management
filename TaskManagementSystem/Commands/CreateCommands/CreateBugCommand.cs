﻿using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.CreateCommands
{
    public class CreateBugCommand : BaseCommand
    {
        private const int minArguments = 5;

        public CreateBugCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - title
            //  [1] - description
            //  [2] - status
            //  [3] - priority
            //  [4] - severity
            //  [5] - assigneeID

            string title = CommandParameters[0];
            string description = CommandParameters[1];
            Status status = ParseEnumOfType<Status>(CommandParameters[2]);
            Priority priority = ParseEnumOfType<Priority>(CommandParameters[3]);
            Severity severity = ParseEnumOfType<Severity>(CommandParameters[4]);
            IMember assignee = null;

            if (CommandParameters.Count == 6)
            {
                int id = ParseIntParameter(CommandParameters[5], "Assignee ID");
                assignee = Repository.FindMemberById(id);
            }

            var bug = Repository.CreateBug(title, description, status, priority, severity, assignee);


            var result = $"Bug with ID: {bug.Id} was created.";

            if (assignee != null)
            {
                result += $" Assignee: {assignee.Name}";
            }

            return result;
        }
    }
}
