﻿using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;

namespace TaskManagementSystem.Commands.CreateCommands
{
    public class CreateMemberCommand : BaseCommand
    {
        private const int minArguments = 1;

        public CreateMemberCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - board name
            //  [1] - team name

            string name = CommandParameters[0];

            var member = Repository.CreateMember(name);

            return $"Member {name} (ID: {member.Id}) was created.";
        }
    }
}
