﻿using System;
using System.Collections.Generic;
using TaskManagementSystem.Commands.Contracts;
using TaskManagementSystem.Core.Contracts;

namespace TaskManagementSystem.Commands
{
    public abstract class BaseCommand : ICommand
    {
        protected BaseCommand(IRepository repository)
            : this(new List<string>(), repository)
        {
        }

        protected BaseCommand(IList<string> commandParameters, IRepository repository)
        {
            this.CommandParameters = commandParameters;
            this.Repository = repository;
        }

        public abstract string Execute();

        protected IRepository Repository { get; }

        protected IList<string> CommandParameters { get; }

        protected int ParseIntParameter(string value, string parameterName)
        {
            if (!int.TryParse(value, out int result))
            {
                throw new ArgumentException($"Invalid value for {parameterName}. Should be an integer number.");
            }

            return result;
        }

        protected void ValidateNumberOfArguments(int minNumber)
        {
            if (this.CommandParameters.Count < minNumber)
            {
                throw new ArgumentException($"Invalid number of arguments. Should be at least {minNumber}.");
            }
        }

        protected TEnum ParseEnumOfType<TEnum>(string value) where TEnum : struct, Enum
        {
            if (!(Enum.TryParse(value, true, out TEnum parsedEnum) && Enum.IsDefined(typeof(TEnum), parsedEnum)))
            {
                throw new ArgumentException($"{typeof(TEnum).Name} does not have {value}.");
            }

            return parsedEnum;
        }
    }
}
