﻿using System;
using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Commands.AssignCommands
{
    public class AssignTaskCommand : BaseCommand
    {
        private const int minArguments = 2;

        public AssignTaskCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - Task id
            //  [1] - Member id

            int taskId = ParseIntParameter(CommandParameters[0], "Task Id");
            int memberId = ParseIntParameter(CommandParameters[1], "Member Id");
            var task = Repository.FindTaskById(taskId);
            var member = Repository.FindMemberById(memberId);

            if (task is IHasAssignee)
            {
                IHasAssignee taskWithAssignee = task as IHasAssignee;
                taskWithAssignee.Assignee = member;
                member.AssignTask(task);
            }
            else
            {
                throw new ArgumentException($"Cannot assign member to task with ID: {taskId}");
            }

            return $"Task with ID: {taskId} was assigned to member with ID: {memberId}.";
        }
    }
}
