﻿using System;
using System.Collections.Generic;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Commands.AssignCommands
{
    public class UnassignTaskCommand : BaseCommand
    {
        private const int minArguments = 2;

        public UnassignTaskCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - Task id
            //  [1] - Member id

            int taskId = ParseIntParameter(CommandParameters[0], "Task Id");
            int memberId = ParseIntParameter(CommandParameters[1], "Member Id");
            var task = Repository.FindTaskById(taskId);
            var member = Repository.FindMemberById(memberId);
            bool successful = false;

            if (task is IHasAssignee)
            {
                IHasAssignee taskWithAssignee = task as IHasAssignee;
                if (taskWithAssignee.Assignee == member)
                {
                    taskWithAssignee.Assignee = null;
                    member.UnassignTask(task);

                    successful = true;
                }
            }

            if (!successful)
            {
                throw new ArgumentException($"Cannot unassign member from task with ID: {taskId}");
            }

            return $"Task with ID: {taskId} was unassigned from member with ID: {memberId}.";
        }
    }
}
