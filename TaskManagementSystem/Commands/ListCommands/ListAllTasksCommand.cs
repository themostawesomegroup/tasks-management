﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models.Contracts;

namespace TaskManagementSystem.Commands.ListCommands
{
    public class ListAllTasksCommand : BaseCommand
    {
        private const int minArguments = 0;

        public ListAllTasksCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - title (to filter by) - OPTIONAL

            var tasks = new List<ITask>(this.Repository.Tasks);

            if (CommandParameters.Count > 0)
            {
                string titleToFilterBy = CommandParameters[0];

                tasks = tasks.Where(t => t.Title.ToLower().Contains(titleToFilterBy.ToLower())).ToList();
            }

            if (tasks.Count == 0)
            {
                // TODO fix this
                return "No tasks to show.";
            }

            var sb = new StringBuilder();

            string taskOutPutFormat = "{0} - {1} with ID: {2}";

            foreach (var t in tasks.OrderBy(t => t.Title))
            {
                sb.AppendLine(string.Format(taskOutPutFormat, t.Title, t.GetType().Name, t.Id));
            }

            return sb.ToString().Trim();
        }
    }
}
