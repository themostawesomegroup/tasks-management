﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ListCommands
{
    public class ListAllTasksOfTypeCommand : BaseCommand
    {
        // TODO
        private const int minArguments = 1;

        public ListAllTasksOfTypeCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            var tasks = new List<ITask>(this.Repository.Tasks);

            // Parameters:
            //  [0] - type
            //  [1] - Status
            //  [2] - Status value
            //  [3] - Assignee
            //  [4] - Assignee value


            //Listalltasksoftype bug status active assignee Anton

            // TODO
            string type = CommandParameters[0];


            // TODO - check if works
            var tasksOfType = new List<ITask>();

            if (type == "bug")
            {
                tasksOfType = tasks.Where(t => t is Bug).ToList();
            }
            else if (type == "story")
            {
                tasksOfType = tasks.Where(t => t is Story).ToList();
            }
            else if (type == "feedback")
            {
                tasksOfType = tasks.Where(t => t is Feedback).ToList();
            }


            for (int i = 2; i < CommandParameters.Count; i += 2)
            {
                string filterWord = CommandParameters[i - 1];
                string value = CommandParameters[i];

                if (filterWord.ToLower() == "status")
                {
                    Status status = base.ParseEnumOfType<Status>(value);
                    tasksOfType = FilterByStatus(tasksOfType, status);
                }
                else if(filterWord.ToLower() == "assignee")
                {
                    string assigneeName = value;
                    tasksOfType = FilterByAssignee(tasksOfType, assigneeName);
                }
            }

            if (tasksOfType.Count == 0)
            {
                return "No tasks found.";
            }

            var sb = new StringBuilder();
            tasksOfType.ForEach(t => sb.AppendLine(t.ToString()));
            return " Tasks : \n" + sb.ToString();
        }

        private List<ITask> FilterByAssignee(List<ITask> tasks, string assigneeName)
        {
            return tasks
                .Select(t => t as IHasAssignee)
                .Where(t => t != null && t.Assignee.Name.ToLower() == assigneeName.ToLower())
                .Select(t => t as ITask)
                .ToList();
        }

        private List<ITask> FilterByStatus(List<ITask> tasks, Status status)
        {
            return tasks.Where(t => t.Status == status).ToList();            
        }
    }
}
