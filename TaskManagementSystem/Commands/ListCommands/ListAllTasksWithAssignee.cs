﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManagementSystem.Core.Contracts;
using TaskManagementSystem.Models.Contracts;
using TaskManagementSystem.Models.Enums;

namespace TaskManagementSystem.Commands.ListCommands
{
    public class ListAllTasksWithAssigneeCommand : BaseCommand
    {
        // TODO
        private const int minArguments = 2;

        public ListAllTasksWithAssigneeCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
            ValidateNumberOfArguments(minArguments);
        }

        public override string Execute()
        {
            // Parameters:
            //  [0] - assignee name
            //  [1] - value
            //  [2] - status
            //  [3] - value

            List<ITask> tasksOfType = Repository.Tasks.Where(t => t is IHasAssignee).ToList(); ;



            for (int i = 1; i < CommandParameters.Count; i += 2)
            {
                string filterWord = CommandParameters[i - 1];
                string value = CommandParameters[i];

                if (filterWord.ToLower() == "status")
                {
                    Status status = base.ParseEnumOfType<Status>(value);
                    tasksOfType = FilterByStatus(tasksOfType, status);
                }
                else if (filterWord.ToLower() == "assignee")
                {
                    string assigneeName = value;
                    tasksOfType = FilterByAssignee(tasksOfType, assigneeName);
                }
            }

            if (tasksOfType.Count == 0)
            {
                return "No tasks found.";
            }

            var sb = new StringBuilder();
            tasksOfType.ForEach(t => sb.AppendLine(t.ToString()));
            return " Tasks : \n" + sb.ToString();
        }

        private List<ITask> FilterByAssignee(List<ITask> tasks, string assigneeName)
        {
            return tasks
                .Select(t => t as IHasAssignee)
                .Where(t => t != null && t.Assignee.Name.ToLower() == assigneeName.ToLower())
                .Select(t => t as ITask)
                .ToList();
        }

        private List<ITask> FilterByStatus(List<ITask> tasks, Status status)
        {
            return tasks.Where(t => t.Status == status).ToList();
        }
    }
}
